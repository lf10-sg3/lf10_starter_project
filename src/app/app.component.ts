import { Component } from '@angular/core';
import { Employee } from "./Employee";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { Globals } from './Globals'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ Globals ]
})
export class AppComponent {

  employees$: Observable<Employee[]>;

  constructor(private http: HttpClient, public globals: Globals) {
    this.employees$ = of([]);
    this.fetchData();
  }

  fetchData() {
    this.employees$ = this.http.get<Employee[]>('/backend', {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.globals.bearer}`)
    });
  }

}
