import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import {LogoutDialogComponent} from "../logout-dialog/logout-dialog.component";
import { MatDialog } from '@angular/material/dialog';
import { LoginService } from "../../services/login.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  getLoggedIn() {
    return sessionStorage.getItem('loggedIn');
  }

  constructor(private location: Location,
              private dialog: MatDialog,
              private loginService: LoginService,
              private router: Router) {}

  ngOnInit(): void {}

  logout() {
    const dialogRef = this.dialog.open(LogoutDialogComponent, {
      width: '400px',
    });

    dialogRef.afterClosed().subscribe((result: boolean) => {
      if (result) {
        sessionStorage.setItem('loggedIn', 'no');
        this.router.navigate(['login']);
      } else {
        console.log('still logged in');
      }
    });
  }

}
