import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { DeleteDialogComponent } from '../delete-dialog/delete-dialog.component';
import { EditDialogComponent } from '../edit-dialog/edit-dialog.component';
import { EmployeeServiceService } from '../../services/employee-service.service';
import { Employee } from '../../Employee';
import { SingleViewComponent } from '../single-view/single-view.component';
import { AddEmployeeComponent } from '../add-employee/add-employee.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent {

  displayColumns: string[] = [
    'id',
    'lastName',
    'firstName',
    // 'street',
    // 'postcode',
    // 'city',
    // 'phone',
    'delete',
    'edit',
  ];

  // states
  isLoadingResults: boolean = true;
  dataSource!: MatTableDataSource<Employee>;

  selectedValue: string = 'byName';

  @ViewChild('paginator') paginator!: MatPaginator;

  constructor(
    private dialog: MatDialog,
    private employeeService: EmployeeServiceService
  ) {}

  private compare(a: Employee, b: Employee) {
    if ( a.id! < b.id! ){
      return -1;
    }

    if ( a.id! > b.id! ){
      return 1;
    }

    return 0;
  }

  private refreshData() {
    if(!this.isLoadingResults) {
      this.isLoadingResults = true;
    }

    this.employeeService.fetchEmployees().subscribe((emps: Employee[]) => {
      if(emps !== undefined){
        this.dataSource.data = emps.sort(this.compare);
        this.isLoadingResults = false;
      }
    });
  }

  ngOnInit(): void {
    this.employeeService.fetchEmployees().subscribe((emps: Employee[]) => {
      this.dataSource = new MatTableDataSource(emps);
      // this.ref.detectChanges();
      this.dataSource.data = emps.sort(this.compare);
      this.dataSource.paginator = this.paginator;
      this.isLoadingResults = false;
    });
  }

  selectUser(user: Employee) {
    // navigate to details
    console.log('selected ' + user.firstName);
    const dialogRef = this.dialog.open(SingleViewComponent, {
      width: '100vw',
      height: '100vh',
      data: user,
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      setTimeout(() => this.refreshData(), 250);
    });
  }

  createUser() {
    console.log('create user');
    const dialogRef = this.dialog.open(AddEmployeeComponent, {
      width: '100vw',
      height: '100vh'
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (typeof result === 'string') {
        console.log(result);
        this.refreshData();
      } else {
        console.log(result);
        this.employeeService.createEmployee(result);
        this.isLoadingResults = true;
        setTimeout(() => this.refreshData(), 500);
      }
    });
  }

  editUser(user: Employee) {
    console.log('edit user');
    const dialogRef = this.dialog.open(EditDialogComponent, {
      width: '800px',
      data: user,
    });

    dialogRef.afterClosed().subscribe((result: Employee | string) => {
      if (typeof result === 'string') {
        console.log(result);
        this.refreshData();
      } else {
        console.log(result);
        this.employeeService.modifyEmployee(user.id!, result);
        this.isLoadingResults = true;
        setTimeout(() => this.refreshData(), 500);
      }
    });
  }

  delUser(user: Employee) {
    console.log('delete user');
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '400px',
      data: user,
    });

    dialogRef.afterClosed().subscribe((result: Employee | null) => {
      if (result === null) {
        console.log(result);
      } else {
        console.log(result);
        this.employeeService.deleteEmployee(user.id!, result)
        setTimeout(() => this.refreshData(), 500);
      }
    });
  }

  applyFilter(event: Event): void {
    console.log(this.selectedValue);
    if (this.selectedValue === "byId") {
      this.dataSource.filterPredicate = function (record, filter) {
        if(record.id !== undefined)
          return record.id.toString().includes(filter);
        else
          return false;
      }
    }

    if (this.selectedValue === "byName") {
      let searchFields = ['firstName', 'lastName']
      this.dataSource.filterPredicate = function (record) {
        let valueArray: string[];
        let allWords: boolean[] = [];
        valueArray = (event.target as HTMLInputElement).value.split(' ');
        valueArray.forEach(v => {
         if(v !== '') {
           let currentWordMatches = false;
           searchFields.forEach((f, i) =>
               currentWordMatches = (record[f as keyof Employee]?.toString().toLowerCase().includes(v.trim().toLowerCase())) || currentWordMatches
           )
           allWords.push(currentWordMatches);
         }
        })
        return allWords.reduce((a,b) => a && b);
      }
    }
    let filterVal = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterVal.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
      this.dataSource.paginator.getNumberOfPages();
    }
  }
}
