import { Component} from '@angular/core';
import { MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-logout-dialog',
  templateUrl: './logout-dialog.component.html',
  styleUrls: ['./logout-dialog.component.css']
})
export class LogoutDialogComponent {

  constructor(private dialogRef: MatDialogRef<LogoutDialogComponent>) { }

  yesClick(): void {
    this.dialogRef.close(true);
  }

  noClick(): void {
    this.dialogRef.close(false);
  }

}
