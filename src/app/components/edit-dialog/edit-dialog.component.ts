import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Employee } from 'src/app/Employee';

@Component({
  selector: 'app-edit-dialog',
  templateUrl: './edit-dialog.component.html',
  styleUrls: ['./edit-dialog.component.css'],
})
export class EditDialogComponent {

  postCodeError: boolean = false;
  constructor(
    private dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Employee
  ) {}

  ngOnInit(): void {}

  editClick(): void {
    if(this.data.postcode?.length !== 5) {
      this.postCodeError = true;
      return
    }
    this.dialogRef.close(this.data);
  }

  cancelClick(): void {
    this.dialogRef.close('canceled');
  }

}
