import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { catchError, Observable, of, tap } from "rxjs";
import { MessageServiceService } from './message-service.service';
import { Qualification } from '../Qualification';
import { ID_FN_LN_Employee } from './employee-service.service';
import { LoginService } from './login.service';

export interface QualifedEmployees{
  designation?: string,
  employees?: ID_FN_LN_Employee
}

@Injectable({
  providedIn: 'root'
})
export class QualificationService {

  baseUrl = '/qualifications';

  constructor(private http: HttpClient, private messageService: MessageServiceService, private lg: LoginService) {}

  getQualifications(): Observable<Qualification> {
     return this.http.get(this.baseUrl, { headers: this.lg.getAuthHeader()}).pipe(
      tap(_ => this.log(`got all Qqalifications`)),
      catchError(this.handleError<any>('getQualifications'))
    );
   }

  CreateQualifications(xQualification: Qualification): Observable<Qualification> {
    let Sendbody = JSON.stringify(xQualification);

    return this.http.post(this.baseUrl,Sendbody, { headers: this.lg.getAuthHeader()}).pipe(
      tap(_ => this.log(`created qualification ${xQualification.designation}`)),
      catchError(this.handleError<any>('createQualification'))
   );
  }

  deleteQualifications(xQualification: Qualification) {
    let SendOptions = {
      headers: this.lg.getAuthHeader(),
      body: {
        designation: xQualification.designation
      }
    }

    this.http.delete(this.baseUrl,SendOptions).pipe(
      tap(_ => this.log(`deleted qualification ${xQualification.designation}`)),
      catchError(this.handleError<any>('deleteQualification'))
   );
  }

  findEmployeesbyQualification(): Observable<QualifedEmployees> {
    return this.http.get(this.baseUrl, { headers: this.lg.getAuthHeader()}).pipe(
      tap(_ => this.log(`Got qualification`)),
      catchError(this.handleError<any>('getQualification'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    };
  }

  private log(message: string) {
    this.messageService.add(`QualificationService: ${message}`);
  }

}
