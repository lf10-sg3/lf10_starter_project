import { Injectable } from '@angular/core';
import { Employee } from "../Employee";
import { HttpClient } from "@angular/common/http";
import { catchError, Observable, of, tap } from "rxjs";
import { MessageServiceService } from './message-service.service';
import { Qualification } from '../Qualification';
import { Globals } from '../Globals';
import { LoginService } from './login.service';

export interface guttedEmpolyee {
  id?: number,
  lastName?: string,
  firstName?: string,
  skillSet?: Qualification[]
}
export interface ID_FN_LN_Employee {
  id?: number,
  lastName?: string,
  firstName?: string
}
@Injectable({
  providedIn: 'root'
})
export class EmployeeServiceService {

  employeeControllerURL = '/backend';

  constructor(private http: HttpClient, private messageService: MessageServiceService, public globals: Globals, public lg: LoginService) {}

  fetchEmployees(): Observable<Employee[]>  {
    return this.http.get<Employee[]>('/backend', { headers: this.lg.getAuthHeader()}).pipe(
      tap(_ => this.log(`fetched all employees`)),
      catchError(this.handleError<any>('fetchEmployees'))
    );
  }

  modifyEmployee(xID: number, xEmployee: Employee){
    let SendBody = {
      "id": xID,
      "firstName": xEmployee.firstName,
      "lastName": xEmployee.lastName,
      "street": xEmployee.street,
      "postcode": xEmployee.postcode,
      "city": xEmployee.city,
      "phone": xEmployee.phone
    };

    this.http.put(`${this.employeeControllerURL}/${xID}`, SendBody, { headers: this.lg.getAuthHeader()}).pipe(
      tap(_ => this.log(`updated employee id=${xID}`)),
      catchError(this.handleError<any>('modifyEmployee'))
    ).subscribe({ next: () => console.log(`updated employee id=${xID}`), error: (err) => console.log(err) });
  }

  deleteEmployee(xID: number, xEmployee: Employee){
    this.http.delete(`${this.employeeControllerURL}/${xID}`, { headers: this.lg.getAuthHeader()}).pipe(
      tap(_ => this.log(`deleted employee id=${xID}`)),
      catchError(this.handleError<any>('deleteEmployee'))
    ).subscribe({ next: () => console.log(`${xEmployee.firstName} deleted`), error: (err) => console.log(err) });
  }

  fetchSingleEmployee(xID: number): Observable<Employee> {
    return this.http.get(`${this.employeeControllerURL}/${xID}}`, { headers: this.lg.getAuthHeader()}).pipe(
      tap(_ => this.log(`fetched all employees`)),
      catchError(this.handleError<any>('fetchEmployees'))
    );
  }

  createEmployee(xEmployee: Employee){
    const URL = this.employeeControllerURL;
    let SendBody = JSON.stringify({
      firstName: xEmployee.firstName,
      lastName: xEmployee.lastName,
      street: xEmployee.street,
      postcode: xEmployee.postcode,
      city: xEmployee.city,
      phone: xEmployee.phone
    });

    this.http.post(URL, SendBody, { headers: this.lg.getAuthHeader()}).pipe(
      tap(_ => this.log(`created employee`)),
      catchError(this.handleError<any>('createEmployee'))
    ).subscribe({next: () => console.log(`${xEmployee.firstName} created`), error: (err) => console.log(err)});
  }

  findEmployeeQualifications(xID: number): Observable<guttedEmpolyee>{
    return this.http.get(`${this.employeeControllerURL}/${xID}/qualifications`, { headers: this.lg.getAuthHeader()}).pipe(
      tap(_ => this.log(`found qualifications for employee id=${xID}`)),
      catchError(this.handleError<any>('createEmployee'))
    );
  }

  addEmployeeQualification(xID: number, xQualification: Qualification): Observable<guttedEmpolyee> {
    let SendBody = JSON.stringify(xQualification)

    return this.http.post(`${this.employeeControllerURL}/${xID}/qualifications`, SendBody, { headers: this.lg.getAuthHeader()}).pipe(
      tap(_ => this.log(`added qualifications`)),
      catchError(this.handleError<any>('addEmployeeQualifications'))
    );
  }

  deleteEmployeeQualification(xID: number, xQualification: Qualification): Observable<guttedEmpolyee> {
    let SendOptions = {
      headers: this.lg.getAuthHeader(),
      body: {
        designation: xQualification.designation
      }
    };

    return this.http.delete(`${this.employeeControllerURL}/${xID}/qualifications`, SendOptions).pipe(
      tap(_ => this.log(`deleted qualifications ${xQualification.designation} from ID =${xID}`)),
      catchError(this.handleError<any>('deleteEmployeeQualifications'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    this.messageService.add(`EmployeeService: ${message}`);
  }

}
