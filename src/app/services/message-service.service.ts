import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageServiceService {

  messages: string[] = [];

  add(message: string) {
    console.log();
    this.messages.push(message);
  }

  clear() {
    this.messages = [];
  }

}
