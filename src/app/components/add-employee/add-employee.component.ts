import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { Employee } from '../../Employee';
import { EmployeeServiceService } from '../../services/employee-service.service';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-add-employee',
  templateUrl: './add-employee.component.html',
  styleUrls: ['./add-employee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  submitted = false;

  res: Employee = {
    firstName: "",
    lastName: "",
    street: "",
    postcode: "",
    city: "",
    phone: ""
  };

  form = this.formBuilder.group(
    {
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      street: ['', Validators.required],
      postCode: ['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(5)
      ])],
      city: ['', Validators.required],
      phone: ['', Validators.required],
    }
  )

  constructor(
    private dialogRef: MatDialogRef<AddEmployeeComponent>,
    private employeeService: EmployeeServiceService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {}

  cancel(): void {
    this.dialogRef.close('canceled');
  }

  submitEmployee(): void {
    this.submitted = true;
    if (this.form.invalid) {
      return;
    }
    this.employeeService.createEmployee(this.res);
    this.cancel();
  }

}
