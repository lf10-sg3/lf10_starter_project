import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DeleteDialogComponent } from '../delete-dialog/delete-dialog.component';
import { EditDialogComponent } from '../edit-dialog/edit-dialog.component';
import { Employee } from '../../Employee';
import { EmployeeServiceService } from 'src/app/services/employee-service.service';

@Component({
  selector: 'app-single-view',
  templateUrl: './single-view.component.html',
  styleUrls: ['./single-view.component.css']
})
export class SingleViewComponent implements OnInit {

  constructor(
    private dialog: MatDialog,
    private employeeService: EmployeeServiceService,
    private dialogRef: MatDialogRef<EditDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Employee
  ) {}

  ngOnInit(): void {}

  editUser(user: Employee) {
    console.log('edit user');

    const dialogRef = this.dialog.open(EditDialogComponent, {
      width: '800px',
      data: user,
    });

    dialogRef.afterClosed().subscribe((result: Employee | string) => {
      if (typeof result === 'string') {
        console.log(result);
      } else {
        console.log(result);
        this.employeeService.modifyEmployee(user.id!, result);
      }
    });
  }

  delUser(user: Employee) {
    console.log('delete user');

    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: '400px',
      data: user,
    });

    dialogRef.afterClosed().subscribe((result: Employee | null) => {
      if (result === null) {
        console.log(result);
      } else {
        console.log(result);
        this.employeeService.deleteEmployee(user.id!, result);
        this.cancelClick();
      }
    });
  }

  cancelClick(): void {
    this.dialogRef.close('canceled');
  }

}
