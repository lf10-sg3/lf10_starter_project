import { Injectable } from '@angular/core';
import { CanActivate} from "@angular/router";
import { LoginService } from "./login.service";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate{

  constructor(private loginService: LoginService, private router: Router) {}

  canActivate() {
    if (sessionStorage.getItem('loggedIn') === 'yes') {
      return true;
    } else {
      return this.router.navigate(['login']);
    }

  }

}
