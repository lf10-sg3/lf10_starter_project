import { Injectable } from '@angular/core'
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { BearerToken } from "../bearer-token";
import { catchError, Observable, tap, throwError } from "rxjs";
import { HttpHeaders } from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  set loggedIn(value: boolean) {
    this._loggedIn = value;
  }

  get loggedIn(): boolean {
    return this._loggedIn;
  }

  private bearerToken: BearerToken | undefined;
  private url: string  = 'http://authproxy.szut.dev';
  private _loggedIn: boolean = false;

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' })
  };

  constructor(private httpClient: HttpClient) {}

  public getAuthHeader() {
    let a = { 'Authorization': 'Bearer ' + sessionStorage.getItem('bearerToken'), 'Content-Type': 'application/json'};
    console.log(a);

    return a;
  }

  login(username: string, password: string): Observable<BearerToken> {
    let requestBody = `grant_type=password&client_id=employee-management-service&username=${username}&password=${password}`;

    return this.httpClient.post<BearerToken>(this.url, requestBody, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
    }).pipe(
      tap(_ => {_.access_token !== '' ? this._loggedIn = true : this._loggedIn = false;
        this.bearerToken = _;
      }), catchError(this.errorHandler));
  }

  private errorHandler(err: HttpErrorResponse): Observable<never> {
    return throwError(() => err);
  }

}
