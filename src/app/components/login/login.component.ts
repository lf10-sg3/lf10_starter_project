import { Component, OnInit } from '@angular/core';
import { FormBuilder } from "@angular/forms";
import { LoginService } from "../../services/login.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  get errorText(): string {
    return this._errorText;
  }

  set errorText(value: string) {
    this._errorText = value;
  }

  get failedLogin(): boolean {
    return this._failedLogin;
  }

  set failedLogin(value: boolean) {
    this._failedLogin = value;
  }

  loginForm = this.formBuilder.group({
    username: '',
    password: ''
  });

  private _failedLogin: boolean = false;
  private _errorText: string = '';

  constructor(private formBuilder: FormBuilder, private loginService: LoginService, private router: Router) {
    sessionStorage.setItem('loggedIn', 'no');
  }

  ngOnInit(): void {}

  login() {
    this.failedLogin = false;
    this.loginService.login(this.loginForm.get('username')?.value, this.loginForm.get('password')?.value).subscribe({
      next: token => {
        if (this.loginService.loggedIn) {
          sessionStorage.setItem('loggedIn', 'yes');
          sessionStorage.setItem('bearerToken', token.access_token);
          this.router.navigate(['list'])
        }},
      error: err => {this.failedLogin = true;
                     err.status === 400 ? this.errorText = 'Login failed. Please check your username and password and try again.' :
                                                           'Oops, something went wrong. Please try again.';

      }
    });
  }

}
